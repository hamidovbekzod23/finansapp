import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import RegistrationPage from '../views/Registration.vue'
import SignInPage from '../views/SignIn.vue'
import MainPage from '../views/MainPage.vue'
import MyCards from '../views/MyCards.vue'
import MyTransactions from '../views/MyTransactions.vue'
import AddCardPage from '../views/AddCard.vue'
import AddTransaction from '../views/AddTransaction.vue'







Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/register',
    name: 'Registration',
    component: RegistrationPage
  },
  {
    path: '/sign',
    name: 'Sign In',
    component: SignInPage
  },
  {
    path: '/main',
    name: 'Main',
    component: MainPage
  },
  {
    path: '/mytransactions',
    name: 'MyTransactions',
    component: MyTransactions
  },
  {
    path: '/mycards',
    name: 'MyCards',
    component: MyCards
  },
  {
    path: '/addcard',
    name: 'AddCardPage',
    component: AddCardPage
  },
  {
    path: '/addtrans',
    name: 'AddTransaction',
    component: AddTransaction
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
