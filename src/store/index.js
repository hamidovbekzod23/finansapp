// import Vue from 'vue'
// import Vuex from 'vuex'
// // import router from '../router'
// import users from './users'

// Vue.use(Vuex)

// export default new Vuex.Store({
//   state: {
    
//   },
//   getters:{

//   },
//   mutations: {
//   },
//   actions: {
//     // CHECK_ME({ commit }) {
//     //   if(!localStorage.token){
//     //     router.push
//     //   }
//     // }
//   },
//   modules: {
//     users
// }
// })

import Vue from "vue";
import Vuex from "vuex";
import users from './users.js'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        users: [],
        arr: localStorage.arr ?
            JSON.parse(localStorage.arr) : [1, 2, 3, 4, 5, 6, 7],
        cards:[
            {
                name: 'Visa',
                curr: 'USD'
            },
            {
                name: 'Visa',
                curr: 'RUB'
            },
            {
                name: 'Visa',
                curr: 'RUB'
            },
            {
                name: 'Visa',
                curr: 'RUB'
            },
            
        ]
    },
    getters: {
        arr: (state) => state.arr,
        myCards: (state) => state.cards
    },
    mutations: {
        CHANGE_ARR_MUTADE(state, data) {
            console.log(data);
            // state.arr.push({ name: "Alex", lastname: "Adams" });

        },
        PARSE_LOCAL(state) {
            if (localStorage.arr) {
                state.arr = JSON.parse(localStorage.arr);
            }
        },
    },
    actions: {
        POPULATE_USERS({ commit }) {
            commit("CHANGE_ARR_MUTADE")
        },
    },
    modules: {
        users
    }
});