import Vue from "vue";
import Vuex from "vuex";
import api from "../api/routers";
import router from "../router/index";

Vue.use(Vuex);

const state = () => ({
    data: [],
    user: {},
    routes: {
        register: {
            api: "auth/signup"
        },
        login: {
            api: "auth/login"
        },
    },
});

const getters = {
    user: (state) => state.user 
}

const mutations = {
    SEND_USER_INFO(state, data){
        state.user = data
    }

};

const actions = {
    FETCH_USERS({ commit }) {
        api.get({ api: state().routes.getALL.api })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    // commit("POPULATE_USERS", res.data)
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    CHECK_ME({ commit }) {
        // alert('123') 
        if (!localStorage.token) {
            // Редирект по имени компонента
            router.push({ name: "Registration" })
        }
    },
    REGISTER({ commit }) {
        event.preventDefault()

        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {

            obj[key] = value
        })

        api.post({ api: state().routes.register.api, obj: obj })
            .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token

                    // console.log(res.data.email);
                    commit("SEND_USER_INFO", res.data)    

                    router.push({ name: "Main" })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
};

export default {
    state,
    getters,
    mutations,
    actions,
};